# CPL410-Collector

This repository contains files and a setup routing to install the Historian OPCUA Collector on a GE CPL410.  Very little linux knowledge/experience is required in order to use these files.

## Requirements

In order for this installer to work you need to do the following:

1. Factory Reset your CPL410 
    * Consult the GE documentation for how to do this
    * (Optional) You can install the [CPL410-Demos](https://bitbucket.org/gescancimtec/cpl410-demos/src) Repository *Note this has had very little testing done, if you happen to test it, let me know how it went.
2. Ensure your CPL410 has an internet connection
3. Have a working licenses Proficy Historian Server
    * You need to know the IP address of this server
    * This server needs to be on the same subnet as your CPL410 or routable from your CPL410
4. Ensure the date matches between the Historian Server and your CPL410
    * To change the timezone on your CPL410 type `sudo dpkg-reconfigure tzdata` and follow the on screen prompts
    * To change the date or time type `sudo date MMDDhhmmyyyy.ss`
5. Make sure you do not have a firewall blocking the collector from connecting to the Historian Server
6. Disable Strict Client Authentication and Strict Collector Authentication (Historian Administrator -> DataStores Tab -> Security Tab)

## Installation

Asuming you have completed the prerequisites from above, installation of the demo system is simple.  Log into your device using the default username (ge) with the default password (ge).  It is reccomended you immediately change your password by typing `passwd` from the command prompt.  You will be prompted to put in your current password, and then asked to type in a new password, and repeat the password.

In order for the next series of steps to function properly your CPL410 must be connected to the internet.

Once you have changed your password and connected the CPL410 to the internet you can install the collector with the following code:

```Shell
cd ~
git clone https://bitbucket.org/gescancimtec/cpl410-collector.git
cd cpl410-collector/
sudo ./setup.sh
```

After some initial setup the installer will pause to ask you for several pieces of information, this includes:

* Historian Server IP
* Collector Name
* Collector Tag Prefix

Thats it!  Wait patiently for the installation to finish and you should see your collector show up in your Historian administrator.
